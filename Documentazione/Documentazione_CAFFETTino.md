# CAFFETTino

La macchina del caffè open source

![Icona Caffettino](https://bitbucket.org/andrea_folci/arduino-espresso-machine-controller/raw/44cc7c50ce32b4eae6836f24360789feeb858245/Documentazione/Immagini/IconaCaffettINO.png)

di Andrea Folci

*This document is licensed under the GNU General Public License v3.0*
*https://www.gnu.org/licenses/gpl-3.0.en.html*

## Indice

[TOC]

------

# Introducendo CAFFETTino

Il progetto è nato un po’ per gioco: avendo recentemente strutturato la cantina come spazio di lavoro diviso in ufficio ed officina avevo deciso di installare una vecchia macchina del caffè casalinga che avevo trovato inutilizzata nel lato officina, approfittando della vicinanza al rubinetto per il riempimento della tanica.

Trovandosi in una stanza diversa da quella in cui normalmente lavoro è scomodo andare a farsi il caffè, considerando anche il tempo di accensione della macchina e il fatto che l’officina durante l’inverno è anche abbastanza fredda.

Un giorno alcuni amici in visita mi dissero scherzosamente di farla accendere da Alexa, così che potessimo andare a farci il caffè a macchina già calda.

Inoltre la macchina non faceva un caffè particolarmente buono: la macinazione dei grani era insufficiente e la durata dell’erogazione era eccessiva, producendo un risultato molto annacquato. Purtroppo i parametri con cui la macchinetta lavorava non erano modificabili.

Per questi motivi ho deciso di eliminare completamente la scheda della macchinetta e rimpiazzarla con una struttura di controllo basata su Arduino Uno.



Il progetto prevede di trasformare la macchina, una Gaggia modello ‘Unica’, in CAFFETTino, una macchina del caffè che sia completamente configurabile ed espandibile nelle sue funzioni.

Il progetto passa attraverso una fase di reverse engineering delle componenti presenti e della scheda di controllo, la progettazione del circuito di alimentazione degli attuatori e del codice di controllo.

In una prima fase pensavo di utilizzare solo una scheda per la gestione dell’intera macchina, nella sua versione finale ho optato per dividere il lavoro su due schede separate: un Arduino Uno per la gestione dei sensori e degli attuatori tramite relays, un Wemos D1 R2 per l’interfaccia con l’utente. Le due schede comunicano tramite seriale.



Il dispositivo avrà le seguenti funzioni:

- Controllo sia locale tramite pulsantiera che in remoto via WiFi
- Comunicazione dello stato corrente della macchina e dei valori dei sensori accessibili sia dal pannello OLED che da remoto
- Erogazione di Caffè Corto
- Erogazione del "Caffè Imperiale"

------

# Componenti Utilizzate

## Attuatori

![Macinacaffè](https://bitbucket.org/andrea_folci/arduino-espresso-machine-controller/raw/65957b877795e27a73c5ca283b30f2008dca9ecf/Documentazione/Immagini/Componentistica/Macinacaffe.png)

**Macinacaffè**

Basato su motore DOMEL 482.3.531

Tensione d'esercizio 230V DC - Potenza 180W



![Pompa](https://bitbucket.org/andrea_folci/arduino-espresso-machine-controller/raw/65957b877795e27a73c5ca283b30f2008dca9ecf/Documentazione/Immagini/Componentistica/Pompa.jpg)

**Pompa**

Componente:  ULKA Model EP5GW - Pompa a vibrazione

Tensione d’esercizio 230V AC / 50Hz - Potenza 48W



![Caldaia](https://bitbucket.org/andrea_folci/arduino-espresso-machine-controller/raw/65957b877795e27a73c5ca283b30f2008dca9ecf/Documentazione/Immagini/Componentistica/Caldaia.jpg)

**Caldaia**

Componente:  Saeco 11013735 - Caldaia tubolare slim

Tensione d’esercizio 230V AC / 50Hz - Potenza 1300W



![Motoriduttore](https://bitbucket.org/andrea_folci/arduino-espresso-machine-controller/raw/65957b877795e27a73c5ca283b30f2008dca9ecf/Documentazione/Immagini/Componentistica/Motoriduttore.png)

**Motoriduttore**

Componente: Saeco 11005214 -  Motore movimento gruppo              

Tensione d’esercizio 24V DC - Potenza 32W


![OLED](https://bitbucket.org/andrea_folci/arduino-espresso-machine-controller/raw/65957b877795e27a73c5ca283b30f2008dca9ecf/Documentazione/Immagini/Componentistica/OLED.jpg)

**Display OLED**

Componente: Display OLED 0,96” SSD1306

Tensione d’esercizio 3.0 - 5.0V DC - Risoluzione 128x64px

 



## Alimentazione

![Alim24v](https://bitbucket.org/andrea_folci/arduino-espresso-machine-controller/raw/65957b877795e27a73c5ca283b30f2008dca9ecf/Documentazione/Immagini/Componentistica/Alim24v.jpg)

**Alimentatore 24V DC**

Componente: Alimentatore LPV-35-24 - Utilizzato per alimentare il Motoriduttore

Tensione 220V AC IN / 24V DC OUT - Potenza 35W



![Raddrizzatore](https://bitbucket.org/andrea_folci/arduino-espresso-machine-controller/raw/65957b877795e27a73c5ca283b30f2008dca9ecf/Documentazione/Immagini/Componentistica/Rectifier.png)

**Raddrizzatore Ponte di Diodi**

Componente: Raddrizzatore FL408 - Utilizzato per alimentare il Macinacaffè.

Range tensione: 50 - 800 V AC





## Sensori

![Termistore](https://bitbucket.org/andrea_folci/arduino-espresso-machine-controller/raw/65957b877795e27a73c5ca283b30f2008dca9ecf/Documentazione/Immagini/Componentistica/TMP36.jpg)

**Sensore di Temperatura**

Componente: Termistore TMP36 - Rileva la temperatura della caldaia

Tensione d’esercizio: 2.7 - 5.5V DC - Range d’esercizio: - 40°C.  + 125°C.   Err. ± 2 ° C



![Sensore Acqua](https://bitbucket.org/andrea_folci/arduino-espresso-machine-controller/raw/65957b877795e27a73c5ca283b30f2008dca9ecf/Documentazione/Immagini/Componentistica/HCSR04.jpg)

**Sensore livello dell’acqua**

Componente: Sensore ad Ultrasuoni HC-SR04 - Rileva la quantità d'acqua nella tanica 

Tensione d’esercizio 5V DC - Range d’esercizio 2cm - 400cm   Err. ± 3mm



## Controllo

![Arduino](https://bitbucket.org/andrea_folci/arduino-espresso-machine-controller/raw/65957b877795e27a73c5ca283b30f2008dca9ecf/Documentazione/Immagini/Componentistica/ArduinoUno.jpg)

**Arduino UNO R3**

Controllo fisico dei sensori degli attuatori, segue da slave le istruzioni impartitegli dal Wemos tramite protocollo seriale Firmata.

Tensione d’esercizio 5V



![Wemos](https://bitbucket.org/andrea_folci/arduino-espresso-machine-controller/raw/65957b877795e27a73c5ca283b30f2008dca9ecf/Documentazione/Immagini/Componentistica/WemosD1R2.jpg)

**Wemos D1 R2**

Interfaccia IO con l’utente tramite interruttori fisici e protocollo MQTT. Controllo degli stati della macchina comandando l’Arduino tramite protocollo seriale Firmata.

Tensione d’esercizio 3.3V - 5V tolerant nella pratica ma vivamente sconsigliato dal datasheet.



![Relay](https://bitbucket.org/andrea_folci/arduino-espresso-machine-controller/raw/65957b877795e27a73c5ca283b30f2008dca9ecf/Documentazione/Immagini/Componentistica/Relay.jpg)

**Relay KY-019**

 Permette il controllo delle componenti che lavorano a tensioni maggiori dell’Arduino. 

Il pin “Signal” richiede che il GPIO sia in modalità OUTPUT per chiudere l’interruttore.

Tensione d’esercizio: 5V per l’alimentazione - Commuta fino a 250V AC 10A 



------

# Progettazione Hardware

## Controllo di sensori e attuatori

Dato che tutti gli attuatori operano a tensioni superiori ai 5V che la scheda Arduino Uno può offrire, come scelta progettuale ho optato per un controllo mediato da Relays.

Lo schema di alimentazione è come segue:

![Schema Alimentazione Attuatori](https://bitbucket.org/andrea_folci/arduino-espresso-machine-controller/raw/44cc7c50ce32b4eae6836f24360789feeb858245/Documentazione/Immagini/Progettazione%20Hardware/Schema%20Alimentazione%20Attuatori.png)

Ogni attuatore (tranne il motoriduttore) è direttamente collegato alla fase della sorgente d’alimentazione, i relè ne interrompono il neutro. Ciascun relè è alimentato dal 5V dell’Arduino Uno, ogni pin di segnale (in giallo) è connesso al corrispondente GPIO, che deve essere assolutamente impostato come OUTPUT nella fase di setup().

La caldaia e la pompa funzionano entrambe a 220V 50Hz AC, quindi non è stato necessario alcun adattamento dell’alimentazione in entrata. Il macinacaffè richiede una corrente continua a 230V, quindi gli è stato interposto un ponte di diodi per raddrizzare la corrente. Il motoriduttore richiede una corrente continua a 24V, quindi è stato inserito nel circuito un alimentatore che eroga corrente alla tensione e potenza necessaria, lo stesso principio vale per Arduino e Wemos, che richiedono una corrente continua a 5V.



Il gruppo caffè non compie un giro completo, raggiunge infatti la sua fase di infusione con un giro di circa 180°, per poi tornare indietro. Per questo motivo è necessario che al motoriduttore possa essere invertita la polarità al momento opportuno.

![Schema Alimentazione Motoriduttore](https://bitbucket.org/andrea_folci/arduino-espresso-machine-controller/raw/44cc7c50ce32b4eae6836f24360789feeb858245/Documentazione/Immagini/Progettazione%20Hardware/Schema%20Alimentazione%20Motoriduttore.png)

Con questo tipo di implementazione con i relè entrambi rilassati il motoriduttore rimane inattivo, cortocircuitato al polo positivo (con quindi il circuito aperto). Nel caso eccitassi il relè in basso chiuderei il circuito collegando i poli della sorgente in modo diretto con i poli del motoriduttore, causandone un movimento in senso orario. Al contrario, se eccitassi il relè in alto chiuderei il circuito collegando i poli della sorgente in modo inverso con i poli del motoriduttore, causandone un movimento in senso antiorario. Se eccitassi contemporaneamente entrambi i relè il motoriduttore rimarrebbe inattivo, dato che sarebbe cortocircuitato al polo negativo della sorgente.



I sensori utilizzati, un termistore e un misturatore del livello dell’acqua ad ultrasuoni, vengono alimentati direttamente dall’Arduino Uno a 5V. 

I relè e i sensori sono collegati all’Arduino secondo questo schema.

![Schema Arduino Bradboard View](https://bitbucket.org/andrea_folci/arduino-espresso-machine-controller/raw/44cc7c50ce32b4eae6836f24360789feeb858245/Documentazione/Immagini/Progettazione%20Hardware/schema%20arduino_bb.png)



## Interfaccia utente

È stata riutilizzata la pulsantiera originale della macchinetta, creando da zero una struttura in pvc in cui sono stati inseriti i bottoni.

I bottoni sono collegati tra loro tramite jumper wires ad un percorso comune che li porta ad un pin GND del Wemos, e ciascun bottone è collegato ad un GPIO personale. Questa scelta circuitale deriva dal fatto che i GPIO dedicati ai pulsanti sono stati impostati in modalità INPUT_PULLUP, per limitare il fenomeno di bouncing del segnale. 

Al wemos è collegato anche uno schermo OLED 128x64 per visualizzare informazioni utili sullo stato della macchina. È stato inoltre aggiunto uno speakerino per fare segnali acustici e altre funzioni.

I pulsanti e lo speaker sono collegati al wemos secondo questo schema:

![Schema Wemos Bradboard View](https://bitbucket.org/andrea_folci/arduino-espresso-machine-controller/raw/44cc7c50ce32b4eae6836f24360789feeb858245/Documentazione/Immagini/Progettazione%20Hardware/schema%20wemos_bb.png)



------

# Progettazione Software

Inizialmente il progetto aveva come obbiettivo quello di usare una sola scheda, l'Arduino Uno, sia per il controllo dei sensori e attuatori sia per l'interazione con l'utente. Durante la fase progettuale ho optato per una soluzione a due schede, affidando ad un Wemos D1 R2 il compito di fungere da interfaccia con l'utilizzatore.

Questo approccio mi ha permesso di dividere il carico di lavoro e ha aumentato il numero di GPIO a disposizione e quindi di aggiungere altre funzionalità al progetto, quali lo schermo OLED e lo speaker per i segnali acustici.

Lo schermo comunica con Wemos attraverso lo standard I2C, quindi usando due pin: uno per i dati e uno per il clock. La libreria utilizzata per gestirlo è quella fornita da Adafruit: [Adafruit_SSD1306](https://github.com/adafruit/Adafruit_SSD1306).

La scheda Wemos mi permette, grazie al suo chip ESP8266 che ha integrata un'antenna WiFi, di ampliare il modo con cui CAFFETTino si interfaccia con l'utente. Inizialmente solo locale tramite la pulsantiera, il WiFi rende possibile l'interazione anche da remoto. Le comunicazioni si basano sul protocollo MQTT, utilizzando come Broker il server del mio docente, Wemos effettua una publish dello stato della macchina e dei valori dei sensori sul topic 'AndreaFolci/CaffettINO/outputs/'', mentre effettua una subscribe al topic 'AndreaFolci/CaffettINO/inputs/' per ricevere i comandi da remoto. Le librerie che usa per gestire WiFi e MQTT sono [PubSubClient](https://pubsubclient.knolleary.net/) e ESP8266WiFi. Sul telefono utilizzo l'app [MQTT Dashboard](https://play.google.com/store/apps/details?id=com.app.vetru.mqttdashboard), completa e con una buona interfaccia.

L'approccio a due schede introduce il problema della comunicazione tra le stesse: ho deciso di implementare il protocollo [Firmata](https://www.arduino.cc/en/reference/firmata), che mi permette di creare una relazione master/slave tra la scheda Wemos e l'Arduino Uno. Così facendo la scheda Wemos si prende carico, oltre alla gestione dell'I/O con l'utente, anche della gestione degli stati della macchina, modificando l'output dei GPIO di Arduino tramite seriale con [FirmataMarshaller](https://github.com/firmata/arduino/blob/master/FirmataMarshaller.h). Su Arduino è flashata una versione customizzata dello sketch [StandardFirmata](https://github.com/firmata/arduino/blob/master/examples/StandardFirmata/StandardFirmata.ino), in cui sono aggiunte procedure per la lettura dei sensori senza necessità di richiesta da parte di Wemos.

## 

Il codice della gestione CAFFETTino è stato sviluppato come una finite state machine.

All'avvio rimane in ricerca del WiFi finchè non lo trova, dopodichè si mette in uno stato di standby, dove tutti gli attuatori rimangono spenti e la macchina rimane in attesa del segnale di accensione sia in locale che da remoto. Nel frattempo manda al broker MQTT lo stato "Spenta" ogni 15s.

Ricevuto il segnale di accensione chiude il relè della Caldaia, finchè questa non arriva alla temperatura di 90°C. Nel frattempo mostra a schermo e invia al broker MQTT il valore di temperatura della caldaia, aggiornato al secondo.

Una volta concluso il riscaldamento della Caldaia, controlla che ci sia acqua sufficiente nella tanica (valore superiore ai 0,25l) per effettuare un ciclo di riscaldamento del Gruppo Caffè (che consiste nell'erogare acqua bollente per qualche secondo), in caso contrario passa direttamente allo stato idle.

Nello stato idle rimane in attesa di un segnale sia in locale che da remoto, questi possono essere la richiesta di un Caffè Corto, la richiesta di un 'Caffè Imperiale', la richiesta dei dati dei sensori della macchina oppure lo spegnimento di CAFFETTino.

L'erogazione del Caffè prevede l'attivazione del macinacaffè per 10s, il movimento in senso antiorario del gruppo caffè  fino a che non raggiunge la posizione d'infusione, l'erogazione d'acqua per 2s (preinfusione della cialda), l'erogazione d'acqua per 6s (infusione ed erogazione del caffè), infine il movimento in senso orario del gruppo che espelle la cialda usata e si riporta in posizione neutra. Durante questa fase la comunicazione dello stato dei sensori della macchina al Broker MQTT è sospesa in quanto l'accensione e lo spegnimento dei relè altera il valore fornito dal termistore.

La fase di spegnimento prevede l'apertura del relè della Caldaia e, se presente acqua in misura superiore ai 0,25l, un ciclo di pulizia del circuito idraulico e del Gruppo Caffè. Terminati questi passaggi la macchina torna allo stato di standby.



La finite state machine è implementata attraverso l'uso della libreria [TaskScheduler](https://github.com/arkhipenko/TaskScheduler/), che ne gestisce 'nativamente'  la transizione da uno stato all'altro cambiando la callback associata al task. I task che vengono gestiti dallo Scheduler in CAFFETTino sono per gestire I/O dal broker MQTT, quello per gestire i dati in ricezione da Arduino tramite Firmata, quello per gestire gli stati della FSM associata a CAFFETTino, uno stato per gestire la marcia suonata durante l'erogazione del 'Caffè Imperiale'. Nonostante la presenza di interruttori termici sulla caldaia, che aprono il circuito a 105°C. e 125°C. rispettivamente, un ulteriore task è presente che spegne la caldaia sopra i 100°C. e la riaccende sotto i 90°C. I segnali acustici dello speaker sono stati implementati come task per evitare di fare un delay: il task è progettato per essere eseguito due volte, in un intervallo di 300msec. Alla prima esecuzione avvia il segnale acustico, alla seconda lo interrompe.

------

# Futuri sviluppi

Il punto di forza principale di CAFFETTino è l'esser basato sulla piattaforma Arduino, e di poter essere quindi riprogrammato in qualsiasi momento aggiungendo di volta in volta nuove feature. Alcune idee, non implementate nel progetto ma implementabili in futuro sono:

- CAFFETTino la mattina: Inserire un check dell'orario per attivarsi ad un determinato orario.

- CAFFETTino zuccherato: Utilizzando componenti mutuate dai distributori automatici aggiungere un serbatoio di zucchero e di bicchieri esterni alla macchinetta che, attraverso un braccio comandato da un servomotore portano direttamente un bicchiere sotto l'erogatore.

  Si può integrare con CAFFETTino la mattina, facendo trovare per una certa ora un caffè già pronto.

- CAFFETTino jukebox: All'interno della macchina è già presente un Easter Egg, sopra citato come 'Caffè Imperiale'. Non è attivabile con la pulsantiera fisica, ma solo attraverso il Broker MQTT inviando il payload '66' sul topic di input. Questo EE consiste nell'erogare un caffè con il sottofondo della marcia imperiale e visualizzando a schermo la Morte Nera. Futuro sviluppo sarà quello di trasformare questo contenuto nascosto in una vera e propria feature. fornendo all'utente, sia in locale sullo schermo OLED che in remoto via app MQTT, una lista di canzoni permettendogli di scegliere quale ascoltare durante l'erogazione del caffè.

  Si può integrare con CAFFETTino la mattina, impostando una melodia come 'sveglia'.