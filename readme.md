# Arduino Espresso Machine Controller - CAFFETTino

*Autore*: Andrea Folci

*Descrizione*: Fare un operazione di Reverse Engineering su una macchina del caffè domestica Gaggia Unica per sostituirne il circuito di controllo, aggiungendo un sensore di temperatura e di livello dell'acqua consultabili da Client MQTT.

___

*Documentazione Completa*: [Click](./Documentazione/Documentazione_CAFFETTino.md)

*Documentazione in PDF*: [Click](./Documentazione/Documentazione_CAFFETTino.pdf)

___

*Licenza scelta* : GPLv3

## License
Copyright (C) 2020 Andrea Folci

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
