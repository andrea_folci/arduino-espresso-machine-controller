#include <Wire.h>

#define Echo 12 //Echo connnect to pin11
#define Trig 11 //Trig connect to pin12
unsigned long rxTime; //define a variable
float distance; 
float waterLevel;
float waterLiters; 


//TMP36 Pin Variables
int sensorPin = A1; //the analog pin the TMP36's Vout (sense) pin is connected to
                        //the resolution is 10 mV / degree centigrade with a
                        //500 mV offset to allow for negative temperatures
 
/*
 * setup() - this function runs once when you turn your Arduino on
 * We initialize the serial connection with the computer
 */
void setup()
{
  Serial.begin(115200);  //Start the serial connection with the computer
  pinMode(Echo,INPUT);
  pinMode(Trig,OUTPUT);
}
 
void loop()                     // run over and over again
{
 //getting the voltage reading from the temperature sensor
 int reading = analogRead(sensorPin);  
 
 // converting that reading to voltage, for 3.3v arduino use 3.3
 float voltage = reading * 5.0;
 voltage /= 1024.0; 

 // now print out the temperature
 float temperatureC = (voltage - 0.5) * 100 ;  //converting from 10 mv per degree wit 500 mV offset
 Serial.print("TEMPERATURA: ");
 Serial.println(temperatureC);

   // Generates a pulse
  digitalWrite(Trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(Trig, LOW);
  rxTime = pulseIn(Echo, HIGH); //read the Receive time
  //  Serial.print("rxTime:");
  //  Serial.println(rxTime);
  distance = (float)rxTime * 34 / 2000.0; //Converted into a distance ,cm
  waterLevel = 13.6 - distance;

  if(waterLevel <= 2.5)
    waterLiters = waterLevel*0.098;
  else if(waterLevel <= 10.0)
    waterLiters = (waterLevel * 0.121) - 0.0575;
   else if(waterLevel <= 20.0)
    waterLiters = (waterLevel * 0.16) - 0.4475;
  
  if(waterLevel < 800 ) //filter interference signal
  {
    Serial.print("RIEMPIMENTO: ");
    Serial.print(waterLiters); //print it the distance in serial monitor
    Serial.println("l");
  }


 delay(1000);                                     //waiting a second
}
