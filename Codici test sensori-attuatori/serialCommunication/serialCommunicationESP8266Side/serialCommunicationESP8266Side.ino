#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

Adafruit_SSD1306 display = Adafruit_SSD1306(128, 32, &Wire);

String param1, param2;

void setup() {
  Serial.begin(115200);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C); // Address 0x3C for 128x32
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(0,0);
  display.println("INIZIALIZZATO");
  display.display();
  delay(1000);
  }

void loop() {
  param1 = Serial.readStringUntil('\n');
  param2 = Serial.readStringUntil('\n');
  display.clearDisplay();
  display.setCursor(0,0);
  display.println(param1);
  display.println(param2);
  display.display();
  delay(1000);
}
