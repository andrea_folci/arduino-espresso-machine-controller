#include <Wire.h>

#define Echo 12 //Echo connnect to pin11
#define Trig 11 //Trig connect to pin12
unsigned long rxTime; //define a variable
float distance; 
float waterLevel;
float waterLiters; 

void setup() 
{
  // put your setup code here, to run once:
  Serial.begin(115200); //set the baud rate of serial monitor
  pinMode(Echo,INPUT);
  pinMode(Trig,OUTPUT);
}

void loop()
{
  // Generates a pulse
  digitalWrite(Trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(Trig, LOW);
  rxTime = pulseIn(Echo, HIGH); //read the Receive time
  //  Serial.print("rxTime:");
  //  Serial.println(rxTime);
  distance = (float)rxTime * 34 / 2000.0; //Converted into a distance ,cm
  waterLevel = 13.6 - distance;

  if(waterLevel <= 2.5)
    waterLiters = waterLevel*0.098;
  else if(waterLevel <= 10.0)
    waterLiters = (waterLevel * 0.121) - 0.0575;
   else if(waterLevel <= 20.0)
    waterLiters = (waterLevel * 0.16) - 0.4475;
  
  if(waterLevel < 800 ) //filter interference signal
  {
    Serial.print("water level:");
    Serial.print(distance); //print it the distance in serial monitor
    Serial.println("CM");
    Serial.print("- RIEMPIMENTO:");
    Serial.print(waterLiters); //print it the distance in serial monitor
    Serial.println(" litri\n");
    delay(1000);
  }
}
