#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <ESP8266WiFi.h>          //Wifi Antenna Lib
#include <PubSubClient.h>         //MQTT Lib
#include <Firmata.h>
#include <FirmataMarshaller.h>

#define  OLED_PRINT(dim,value, clear)       if(clear)                   \
                                              display.clearDisplay();   \
                                            display.setTextSize(dim);   \
                                            display.print(value);       \
                                            display.display();

#define  OLED_PRINTLN(dim,value, clear)     if(clear)                   \
                                              display.clearDisplay();   \
                                            display.setTextSize(dim);   \
                                            display.println(value);     \
                                            display.display();
                                            
#define OLED_POSITION(x,y)                  display.setCursor(x,y);


#define   ARDUINO_BOILER_PIN                        12
#define   ARDUINO_PUMP_PIN                          10
#define   ARDUINO_GRINDER_PIN                       11
#define   ARDUINO_GEARMOTOR_CLOCKWISE_PIN           8
#define   ARDUINO_GEARMOTOR_COUNTERCLOCKWISE_PIN    9

#define   ARDUINO_GEARMOTOR_STOP_SENSOR_PIN         5
#define   ARDUINO_THERMAL_SENSOR_PIN                A1
#define   ARDUINO_VIRTUAL_WATER_SENSOR_PIN          A5

#define   STARDUST_SONG_TEMPO                       1
#define   WEMOS_TONE_PIN                          D6
#define   WEMOS_CAFFECORTO_PIN                    D3
#define   WEMOS_STATOSENSORI_PIN                  D5
#define   WEMOS_ONOFF_PIN                         D4

#define   TEMPO_ROTAZIONE_GRUPPO                    3700

#define   DELTATEMP_RELE_ATTIVO                     15

firmata::FirmataMarshaller toArduino;

#define   MSG_BUFFER_SIZE  (50)
char msg[MSG_BUFFER_SIZE];

const char*     ssid        =     "Cantina";
const char*     password    =     "OmnesViaemRomamDucunt";
const char*     mqtt_server =     "mqtt.atrent.it";
char            mqttInput   =     '\0';
WiFiClient      espClient;
PubSubClient    client(espClient);
unsigned long   lastMsg     =     0;
int             value = 0;

float           boilerThermalValue;
float           waterLevelValue;
int             stopSensorValue = HIGH;

boolean   stayOn = false;

Adafruit_SSD1306 display = Adafruit_SSD1306(128, 64, &Wire);

void setup_wifi() {
  OLED_PRINT(1, "Connetting to ", true);
  OLED_PRINTLN(1, ssid, false);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    OLED_PRINT(1, ".", false);
  }

  randomSeed(micros());
  
  OLED_POSITION(0,0);
  OLED_PRINTLN(1, "WiFi Connected", true);
  OLED_PRINTLN(1, "IP Address:", false);
  OLED_PRINTLN(1, WiFi.localIP(), false);
  delay(1000);
}

void MQTTCallback(char* topic, byte* payload, unsigned int length) {
  mqttInput = (char)payload[0];
}

void analogWriteCallback(byte pin, int value){
  if(pin == 0){
    boilerThermalValue = value;
  }
  if(pin == 1){
    waterLevelValue = ((float)value)/100.0;
  }  
}

void setup() {
    //INIZIALIZZAZIONE PROTOCOLLI SERIALE
    Firmata.begin();
    Serial.begin(57600);
    toArduino.begin(Serial);

    // INIZIALIZZAZIONE PIN ARDUINO SIDE
    toArduino.sendPinMode(ARDUINO_BOILER_PIN, OUTPUT);
    toArduino.sendPinMode(ARDUINO_PUMP_PIN, OUTPUT);
    toArduino.sendPinMode(ARDUINO_GRINDER_PIN, OUTPUT);
    toArduino.sendPinMode(ARDUINO_GEARMOTOR_CLOCKWISE_PIN, OUTPUT);
    toArduino.sendPinMode(ARDUINO_GEARMOTOR_COUNTERCLOCKWISE_PIN, OUTPUT);
    toArduino.sendPinMode(ARDUINO_GEARMOTOR_STOP_SENSOR_PIN, INPUT_PULLUP);

    // INIZIALIZZAZIONE PIN ESP SIDE    
    pinMode(WEMOS_CAFFECORTO_PIN, INPUT_PULLUP);
    pinMode(WEMOS_STATOSENSORI_PIN, INPUT_PULLUP);
    pinMode(WEMOS_ONOFF_PIN, INPUT_PULLUP);
    pinMode(WEMOS_TONE_PIN, OUTPUT);
    Firmata.attach(ANALOG_MESSAGE, analogWriteCallback);
    //Firmata.attach(DIGITAL_MESSAGE, digitalWriteCallback);
    
    // INIZIALIZZAZIONE SCHERMO OLED
    display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
    display.setTextColor(SSD1306_WHITE);
    
    // INIZIALIZZAZIONE WIFI E MQTT
    setup_wifi();
    client.setServer(mqtt_server, 1883);
    client.setCallback(MQTTCallback);
    if (client.connect("WEMOSClient")) {
          OLED_POSITION(0,0);
          OLED_PRINTLN(1, "CONNESSO AL\nSERVER MQTT", true);
          delay(500);
          OLED_PRINT(1,"",true); //Spegnimento schermo
          client.subscribe("AndreaFolci/CaffettINO/inputs");     
    }

}

void loop () {
  int currentMoment = millis() + 15000;

  //Finchè è spento si limita a mandare al Server MQTT lo stato di Macchina Spenta ogni 15 Secondi e a controllare l'eventuale input di accensione
  while(!stayOn){
    if(millis() - currentMoment > 15000){ //CHECK PER NON INTASARE IL SERVER MQTT
      client.publish("AndreaFolci/CaffettINO/outputs/status/", "Spenta");
      client.publish("AndreaFolci/CaffettINO/outputs/thermal/", "- ");
      client.publish("AndreaFolci/CaffettINO/outputs/waterLevel/", "-");
      currentMoment = millis();
    }

    
    wdt_reset();
    client.loop();    
    if(!digitalRead(WEMOS_ONOFF_PIN) || mqttInput == '3'){
      tone(WEMOS_TONE_PIN, 500);
      delay(500);
      noTone(WEMOS_TONE_PIN);
      mqttInput = '\0';
      OLED_PRINTLN(2, "ACCENSIONE", true);
      stayOn = true;
    }
  }
  
  warmUp();
  idleLoop();
  coolDown();
}

void warmUp(){
   char buffer[64];
   //while(boilerThermalValue < 90.0){

   /*****************************
    * STUB FOR TESTING PURPOSES *
    *****************************/
   int startingPoint = millis();
   while(millis() - startingPoint < 10000){

    
     toArduino.sendDigital(ARDUINO_BOILER_PIN, HIGH);
     client.publish("AndreaFolci/CaffettINO/outputs/status/", "Riscaldamento");
     
     while (Firmata.available()) {
      Firmata.processInput();
     }

     OLED_POSITION(30,5);
     OLED_PRINTLN(1, "RISCALDAMENTO", true);
     delay(1000);
     OLED_POSITION(35,5);
     OLED_PRINTLN(1, "TEMPERATURA",true);
     OLED_POSITION(40,13);
     OLED_PRINT(1, boilerThermalValue, false);
     OLED_PRINT(1, "/90 C.", false);
     
     snprintf(buffer, sizeof buffer, "%.2f", boilerThermalValue);
     client.publish("AndreaFolci/CaffettINO/outputs/thermal/", buffer);
     snprintf(buffer, sizeof buffer, "%.2f", waterLevelValue);
     client.publish("AndreaFolci/CaffettINO/outputs/waterLevel/", buffer);
     delay(1500);
   }

   OLED_POSITION(0,0);
   OLED_PRINTLN(2, "MACCHINA\nCALDA", true);
   delay(1500);
   if(waterLevelValue < 0.30){
     OLED_POSITION(0,0);
     OLED_PRINTLN(2, "RIEMPIRE\nTANICA!", true);
     delay(3000);
   }
   else{
     OLED_POSITION(0,0);
     OLED_PRINTLN(2, "RISCALDO\nGRUPPO CAFFÈ", true);
     
     //PORTO IL GRUPPO IN POSIZIONE DI INFUSIONE
     toArduino.sendDigital(ARDUINO_GEARMOTOR_COUNTERCLOCKWISE_PIN, HIGH);
     delay(TEMPO_ROTAZIONE_GRUPPO);
     toArduino.sendDigital(ARDUINO_GEARMOTOR_COUNTERCLOCKWISE_PIN, LOW);

     //RISCALDO IL CIRCUITO IDRAULICO E IL GRUPPO CON L'ACQUA
     toArduino.sendDigital(ARDUINO_PUMP_PIN, HIGH);
     delay(5000);
     toArduino.sendDigital(ARDUINO_PUMP_PIN, LOW);
     delay(1000);
     
     //RIPORTO IL GRUPPO IN POSIZIONE NEUTRA
     toArduino.sendDigital(ARDUINO_GEARMOTOR_CLOCKWISE_PIN, HIGH);
     delay(TEMPO_ROTAZIONE_GRUPPO);
     toArduino.sendDigital(ARDUINO_GEARMOTOR_CLOCKWISE_PIN, LOW);
   }
}

void coolDown(){
  toArduino.sendDigital(ARDUINO_BOILER_PIN, LOW); //SPENGO LA CALDAIA

  //PORTO IL GRUPPO IN POSIZIONE DI INFUSIONE
  toArduino.sendDigital(ARDUINO_GEARMOTOR_COUNTERCLOCKWISE_PIN, HIGH);
  delay(TEMPO_ROTAZIONE_GRUPPO);
  toArduino.sendDigital(ARDUINO_GEARMOTOR_COUNTERCLOCKWISE_PIN, LOW);

  //FACCIO UNA PULIZIA DEL CIRCUITO IDRAULICO
  toArduino.sendDigital(ARDUINO_PUMP_PIN, HIGH);
  delay(5000);
  toArduino.sendDigital(ARDUINO_PUMP_PIN, LOW);
  delay(1000);

  //RIPORTO IL GRUPPO IN POSIZIONE NEUTRA
  toArduino.sendDigital(ARDUINO_GEARMOTOR_CLOCKWISE_PIN, HIGH);
  delay(TEMPO_ROTAZIONE_GRUPPO);
  toArduino.sendDigital(ARDUINO_GEARMOTOR_CLOCKWISE_PIN, LOW);

  //SPENGO L'OLED
  OLED_PRINT(1,"",true);
}

void idleLoop(){
  int currentMoment = millis() + 15000; 
  while(stayOn){
    if(millis() - currentMoment > 15000){ //CHECK PER NON INTASARE IL SERVER MQTT
      client.publish("AndreaFolci/CaffettINO/outputs/status/", "In Attesa"); 
      currentMoment = millis();
    }
    
    toArduino.sendDigital(ARDUINO_BOILER_PIN, HIGH); //FAILSAFE
    OLED_POSITION(0,0);
    OLED_PRINTLN(2, "IN ATTESA", true);
    
    wdt_reset();
    client.loop();

    while(Firmata.available())
      Firmata.processInput();
    
    if(!digitalRead(WEMOS_CAFFECORTO_PIN) || mqttInput == '0'){
      tone(WEMOS_TONE_PIN, 500);
      delay(500);
      noTone(WEMOS_TONE_PIN);
      if(waterLevelValue < 0.30){
         OLED_POSITION(0,0);
         OLED_PRINTLN(2, "RIEMPIRE\nTANICA", true);
         delay(3000);
      }
      else{
        client.publish("AndreaFolci/CaffettINO/outputs/status/", "Erogazione Caffè Corto");
        OLED_POSITION(0,0);
        OLED_PRINTLN(2, "EROGAZIONE\nCAFFÈ CORTO", true);
  
        coffeeBrewing();
        OLED_POSITION(0,0);
        OLED_PRINTLN(2, "EROGAZIONE\nCOMPLETATA", true);
        tone(WEMOS_TONE_PIN, 500);
        delay(500);
        noTone(WEMOS_TONE_PIN);
        tone(WEMOS_TONE_PIN, 500);
        delay(500);
        noTone(WEMOS_TONE_PIN);
        tone(WEMOS_TONE_PIN, 500);
        delay(500);
        noTone(WEMOS_TONE_PIN);
        }
        mqttInput = '\0';
    }

    if(!digitalRead(WEMOS_STATOSENSORI_PIN) || mqttInput == '2'){
      tone(WEMOS_TONE_PIN, 500);
      delay(500);
      noTone(WEMOS_TONE_PIN);
      client.publish("AndreaFolci/CaffettINO/outputs/status/", "Lettura Sensori");
      OLED_POSITION(0,0);
      OLED_PRINTLN(2, "CONTROLLO\nSTATO SENSORI", true);
      delay(1000);
      sensorChecking();
      mqttInput = '\0';
    }

    if(!digitalRead(WEMOS_ONOFF_PIN) || mqttInput == '3'){
      tone(WEMOS_TONE_PIN, 500);
      delay(500);
      noTone(WEMOS_TONE_PIN);
      client.publish("AndreaFolci/CaffettINO/outputs/status/", "In Spegnimento");  
      mqttInput = '\0';
      OLED_POSITION(0,0);
      OLED_PRINTLN(2, "SPEGNIMENTO", true);
      delay(1000);
      stayOn = false;
      mqttInput = '\0';
    }
    
  }//End While

}//End IdleLoop (Torna a loop() che procederà al coolDown())

void coffeeBrewing(){

  //Prima implementazione della produzione di caffè, senza l'ausilio del sensore di fine corsa
  
  toArduino.sendDigital(ARDUINO_BOILER_PIN, HIGH); //Failsafe

  //Macino caffè per 10 secondi
  toArduino.sendDigital(ARDUINO_GRINDER_PIN, HIGH);
  delay(10000);
  toArduino.sendDigital(ARDUINO_GRINDER_PIN, LOW);
  delay(1500);

  //Porto il gruppo in posizione di infusione, comprimendo la polvere di caffè
  toArduino.sendDigital(ARDUINO_GEARMOTOR_COUNTERCLOCKWISE_PIN, HIGH);
  while(stopSensorValue == HIGH){
    while(Firmata.available())
      Firmata.processInput();
  }
  toArduino.sendDigital(ARDUINO_GEARMOTOR_COUNTERCLOCKWISE_PIN, LOW);
  delay(1500);

  //Faccio andare la pompa 3 secondi per la preinfusione del caffè, aspetto che si bagni la cialda altri 3 secondi
  toArduino.sendDigital(ARDUINO_PUMP_PIN, HIGH);
  delay(3000);
  toArduino.sendDigital(ARDUINO_PUMP_PIN, LOW);
  delay(3500);

  //Procedo con l'infusione del caffè
  toArduino.sendDigital(ARDUINO_PUMP_PIN, HIGH);
  delay(7000);
  toArduino.sendDigital(ARDUINO_PUMP_PIN, LOW);
  delay(2000);

  //Riporto il gruppo in posizione neutra, buttando via la cialda.
  toArduino.sendDigital(ARDUINO_GEARMOTOR_CLOCKWISE_PIN, HIGH);
  delay(TEMPO_ROTAZIONE_GRUPPO);
  toArduino.sendDigital(ARDUINO_GEARMOTOR_CLOCKWISE_PIN, LOW);
}

void sensorChecking(){
   toArduino.sendDigital(ARDUINO_BOILER_PIN, HIGH); //Failsafe

   //Controllo nuovi dati
    while (Firmata.available()) {
      Firmata.processInput();
    }
   
   OLED_POSITION(0,0);
   
   OLED_PRINT(2, "CALDAIA: ", true);
   OLED_PRINT(1, boilerThermalValue, false);
   OLED_PRINTLN(1, " C.", false);

   OLED_PRINT(2, "\nACQUA: ", false);
   OLED_PRINT(1, waterLevelValue, false);
   OLED_PRINTLN(1, " litri", false);

   //Invio parametri al server MQTT
   char buffer[64];
   snprintf(buffer, sizeof buffer, "%.2f", boilerThermalValue);
   client.publish("AndreaFolci/CaffettINO/outputs/thermal/", buffer);
   snprintf(buffer, sizeof buffer, "%.2f", waterLevelValue);
   client.publish("AndreaFolci/CaffettINO/outputs/waterLevel/", buffer);
   delay(4000);
}
